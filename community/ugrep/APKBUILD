# Contributor: Francesco Camuffo <dev@fmac.xyz>
# Maintainer: Celeste <cielesti@protonmail.com>
maintainer="Celeste <cielesti@protonmail.com>"
pkgname=ugrep
pkgver=7.0.3
pkgrel=0
pkgdesc="Ultra fast grep with interactive query UI and fuzzy search"
url="https://ugrep.com/"
arch="all"
license="BSD-3-Clause"
checkdepends="bash"
makedepends="
	brotli-dev
	bzip2-dev
	bzip3-dev
	linux-headers
	lz4-dev
	pcre2-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/Genivia/ugrep/archive/refs/tags/v$pkgver.tar.gz"

build() {
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--with-bzip3
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
921640ac15934180039510a38f5c7e41492f372dec18a21da7bb5f8cd0cacd3b70ff9e6070525997aef0b3f518b43a415e00876f077ee6c6fc83cdf373b57b26  ugrep-7.0.3.tar.gz
"
